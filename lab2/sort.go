package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func merge(left []int, right []int) []int {
	var result []int
	var lIdx, rIdx int

	for i := 0; i < len(left)+len(right); i++ {

		if lIdx == len(left) {
			result = append(result, right[rIdx:]...)
			break
		} else if rIdx == len(right) {
			result = append(result, left[lIdx:]...)
			break
		} else if left[lIdx] <= right[rIdx] {
			result = append(result, left[lIdx])
			lIdx++
		} else {
			result = append(result, right[rIdx])
			rIdx++
		}
	}
	return result
}

func mergeSort(arr []int) []int {
	var result []int

	count := len(arr)

	switch {
	case count > 1:
		lb := mergeSort(arr[:count/2])
		rb := mergeSort(arr[count/2:])
		result = merge(lb, rb)

	case len(arr) == 1:
		return arr
	}
	return result
}

func main() {
	var arr []int

	inFile, _ := ioutil.ReadFile("sort.in")
	outFile, _ := os.Create("sort.out")
	defer outFile.Close()

	line := strings.Split(string(inFile), "\n")[1]
	raw := strings.Fields(line)
	for i := 0; i < len(raw); i++ {
		num, _ := strconv.Atoi(raw[i])
		arr = append(arr, num)
	}

	outFile.WriteString(strings.Trim(fmt.Sprintf("%v", mergeSort(arr)), "[]"))
}
