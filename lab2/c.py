def insertion_sort(arr: list):
    for j in range(2, len(arr)):
        key = arr[j] # 1
        # вставка в отсортированную последовательность
        # слева от элемента
        i = j - 1
        while i >= 0 and arr[i] >= key:
            arr[i+1] = arr[i]
            i -= 1
        arr[i+1] = key
    return arr


if __name__ == "__main__":
    from random import randint
    test_arr = [randint(-15, 123) for i in range(12)]
    print(test_arr)
    print(insertion_sort(test_arr))
