package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func _merge(left []int, right []int) []int {
	var result []int
	var lIdx, rIdx int

	for i := 0; i < len(left)+len(right); i++ {
		if lIdx < len(left) {
			if rIdx < len(right) {
				if left[lIdx] <= right[rIdx] {
					result = append(result, left[lIdx])
					lIdx++
				} else { // Ai > Aj
					inversions += len(left) - lIdx
					result = append(result, right[rIdx])
					rIdx++
				}
			} else {
				result = append(result, left[lIdx])
				lIdx++
			}
		} else if rIdx < len(right) {
			inversions += len(left) - lIdx
			result = append(result, right[rIdx])
			rIdx++
		}
	}
	return result
}

func mergeSort(arr []int) []int {
	var result []int

	count := len(arr)

	switch {
	case count > 1:
		lb := mergeSort(arr[:count/2])
		rb := mergeSort(arr[count/2:])
		result = _merge(lb, rb)

	case len(arr) == 1:
		return arr
	}
	return result
}

var inversions int

func main() {
	var arr []int

	inFile, _ := ioutil.ReadFile("inversions.in")
	outFile, _ := os.Create("inversions.out")
	defer outFile.Close()

	line := strings.Split(string(inFile), "\n")[1]
	raw := strings.Fields(line)
	for i := 0; i < len(raw); i++ {
		num, _ := strconv.Atoi(raw[i])
		arr = append(arr, num)
	}

	mergeSort(arr)
	outFile.WriteString(fmt.Sprintf("%d\n", inversions))
}
