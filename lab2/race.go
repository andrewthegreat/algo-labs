package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func _merge(left []string, right []string) []string {
	var result []string
	var lIdx, rIdx int

	for i := 0; i < len(left)+len(right); i++ {
		if lIdx < len(left) {
			if rIdx < len(right) {
				if left[lIdx] <= right[rIdx] {
					result = append(result, left[lIdx])
					lIdx++
				} else { // Ai > Aj
					result = append(result, right[rIdx])
					rIdx++
				}
			} else {
				result = append(result, left[lIdx])
				lIdx++
			}
		} else if rIdx < len(right) {
			result = append(result, right[rIdx])
			rIdx++
		}
	}
	return result
}

func mergeSort(arr []string) []string {
	var result []string

	count := len(arr)

	switch {
	case count > 1:
		lb := mergeSort(arr[:count/2])
		rb := mergeSort(arr[count/2:])
		result = _merge(lb, rb)

	case len(arr) == 1:
		return arr
	}
	return result
}

type Person struct {
	Name    string
	Country string
}

func main() {
	var countries []string
	var people []Person

	inFile, _ := os.Open("race.in")
	defer inFile.Close()

	scanner := bufio.NewScanner(inFile)
	scanner.Scan()

	countryString := ""
	for scanner.Scan() {
		line := strings.Fields(scanner.Text())
		if !(strings.Contains(countryString, line[0])) {
			countries = append(countries, line[0])
			countryString += line[0]
		}

		var person Person = Person{
			Name:    line[1],
			Country: line[0],
		}
		people = append(people, person)
	}

	outFile, _ := os.Create("race.out")
	defer outFile.Close()

	countries = mergeSort(countries)

	for i := range countries {
		outFile.WriteString(fmt.Sprintf("=== %s ===\n", countries[i]))
		for j := range people {
			if people[j].Country == countries[i] {
				outFile.WriteString(fmt.Sprintf("%s\n", people[j].Name))
			}
		}
	}
}
