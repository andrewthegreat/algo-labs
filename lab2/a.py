def insertion_sort(arr: list):
    for j in range(2, len(arr)):
        key = arr[j]
        i = j -1
        while i >= 0 and arr[i] >= key:
            arr[i+1] = arr[i]
            i -= 1
        arr[i+1] = key
    return arr


with open("sort.in") as in_f:
    in_f.readline()
    arr = list(map(int, in_f.readline().split()))
    insertion_sort(arr)
    result = ""
    for i in range(len(arr)):
        result += f"{arr[i]} "
    result = result.rstrip()
    with open("sort.out", "w") as out_f:
        out_f.write(result)
