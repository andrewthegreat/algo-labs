package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

type Stack struct {
	values []string
}

func (st *Stack) isEmpty() bool {
	if len(st.values) != 0 {
		return false
	}
	return true
}

func (st *Stack) Push(value string) {
	st.values = append(st.values, value)
}

func (st *Stack) Pop() string {
	var last string
	if st.isEmpty() {
		return ""
	}

	last = st.values[len(st.values)-1]
	st.values = st.values[:len(st.values)-1]
	return last
}

func (st *Stack) Top() string {
	if st.isEmpty() {
		return ""
	}
	return st.values[len(st.values)-1]
}

func checkBrackets(input string) string {
	var stack, brackets Stack

	for idx, char := range input {
		if (char == '(') || (char == '[') || (char == '{') {
			stack.Push(string(char))
			brackets.Push(strconv.Itoa(idx + 1))
		} else if (char == ')') || (char == ']') || (char == '}') {
			top := stack.Top()
			if stack.isEmpty() && ((char == ')') || (char == ']') || (char == '}')) {
				return "NO"
			} else if ((top == "(") && (char != ')')) || ((top == "[") && (char != ']')) || ((top == "{") && (char != '}')) {
				return "NO"
			} else {
				stack.Pop()
				brackets.Pop()
			}
		}
	}

	if stack.isEmpty() {
		return "YES"
	} else {
		return "NO"
	}
}

func main() {
	inFile, _ := ioutil.ReadFile("brackets.in")
	outFile, _ := os.Create("brackets.out")

	line := strings.Split(string(inFile), "\n")
	line = line[:len(line)-1] // чтоб избежать лишнего "\n" в конце

	for i := range line {
		outFile.WriteString(checkBrackets(line[i]) + "\n")
	}

	outFile.Close()
}
