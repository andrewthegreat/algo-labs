package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Stack struct {
	values []int
}

func (st *Stack) isEmpty() bool {
	if len(st.values) == 0 {
		return true
	}
	return false
}

func (st *Stack) Push(value int) {
	st.values = append(st.values, value)
}

func (st *Stack) Pop() int {
	if st.isEmpty() {
		return -1
	}
	var last int = st.values[len(st.values)-1]
	st.values = st.values[:len(st.values)-1]
	return last
}

func (st *Stack) Top() int {
	if st.isEmpty() {
		return st.values[len(st.values)-1]
	}
	return 0
}

func main() {
	var stack Stack

	inFile, _ := os.Open("stack.in")
	defer inFile.Close()
	outFile, _ := os.Create("stack.out")

	scanner := bufio.NewScanner(inFile)
	scanner.Scan() // skip first line ("M")

	for scanner.Scan() {
		line := strings.Fields(scanner.Text())
		if line[0] == "+" {
			num, _ := strconv.Atoi(line[1])
			stack.Push(num)
		} else {
			if !(stack.isEmpty()) {
				outFile.WriteString(fmt.Sprintf("%d", stack.Pop()) + "\n")
			}
		}
	}
	outFile.Close()
}
