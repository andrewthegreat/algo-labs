package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Queue struct {
	values []int
}

func (q *Queue) isEmpty() bool {
	if len(q.values) == 0 {
		return true
	}
	return false
}

func (q *Queue) Enqueue(key int) {
	if len(q.values) == 0 {
		q.values = append(q.values, key)
	} else {
		q.values = append(q.values[:1], q.values[0:]...)
		q.values[0] = key
	}
}

func (q *Queue) Dequeue() int {
	if q.isEmpty() {
		return 0
	}
	var last int = q.values[len(q.values)-1]
	q.values = q.values[:len(q.values)-1]
	return last
}

func main() {
	var q Queue
	inFile, _ := os.Open("queue.in")
	defer inFile.Close()
	outFile, _ := os.Create("queue.out")

	scanner := bufio.NewScanner(inFile)
	scanner.Scan()

	for scanner.Scan() {
		line := strings.Fields(scanner.Text())
		switch len(line) {
		case 2:
			key, _ := strconv.Atoi(line[1])
			q.Enqueue(key)

		case 1:
			outFile.WriteString(fmt.Sprintf("%d", q.Dequeue()) + "\n")
		}
	}
	outFile.Close()
}
