package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func binSearchFirst(arr []int, key int) int {
	var left int = -1
	var right int = len(arr)

	for left < right-1 {
		mid := (left + right) / 2
		if arr[mid] == key {
			return mid + 1
		}
		if arr[mid] < key {
			left = mid
		} else {
			right = mid
		}
	}
	return -1
}

func binSearchLast(arr []int, key int) int {
	var last int = -1
	var left int = 0
	var right int = len(arr) - 1

	for left <= right {
		mid := (left + right) / 2
		if arr[mid] == key {
			left = mid + 1
			last = mid
		} else if arr[mid] > key {
			right = mid - 1
		} else {
			left = mid + 1
		}
	}
	return last
}

func find(arr []int, key int) (int, int) {
	first := binSearchFirst(arr, key)
	if first == -1 {
		return -1, -1
	}
	last := binSearchLast(arr, key)
	return first, last + 1
}

func main() {
	var arr, keys []int
	var dataRaw []string

	inFile, _ := ioutil.ReadFile("binsearch.in")
	outFile, _ := os.Create("binsearch.out")
	defer outFile.Close()

	data := strings.Split(string(inFile), "\n")

	dataRaw = strings.Fields(data[1])
	for i := 0; i < len(dataRaw); i++ {
		num, _ := strconv.Atoi(string(dataRaw[i]))
		arr = append(arr, int(num))
	}

	dataRaw = strings.Fields(data[3])
	for i := 0; i < len(dataRaw); i++ {
		num, _ := strconv.Atoi(string(dataRaw[i]))
		keys = append(keys, int(num))
	}

	for i := range keys {
		first, last := find(arr, keys[i])
		fmt.Printf("%d %d\n", first, last)
		// outFile.WriteString(fmt.Sprintf("%d %d\n", first, last))
	}
}
