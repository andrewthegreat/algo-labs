package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

type Stack struct {
	values []int
}

func (st *Stack) isEmpty() bool {
	if len(st.values) == 0 {
		return true
	}
	return false
}

func (st *Stack) Push(value int) {
	st.values = append(st.values, value)
}

func (st *Stack) Pop() int {
	if st.isEmpty() {
		return 0
	}
	var last int = st.values[len(st.values)-1]
	st.values = st.values[:len(st.values)-1]
	return last
}

func main() {
	var stack Stack
	var result int

	inFile, _ := ioutil.ReadFile("postfix.in")
	outFile, _ := os.Create("postfix.out")
	defer outFile.Close()

	// query := strings.Split(string(inFile), " ") // было так
	query := strings.Fields(string(inFile)) // стало так
	for _, item := range query {
		intItem, err := strconv.Atoi(item)
		if err == nil {
			stack.Push(intItem)
		} else {
			operand1 := stack.Pop()
			operand2 := stack.Pop()
			switch item {
			case "+":
				result = operand1 + operand2

			case "*":
				result = operand1 * operand2

			case "-":
				result = operand2 - operand1
			}
			stack.Push(result)
		}
	}
	outFile.WriteString(fmt.Sprintf("%d\n", result))
}
