using System;

class TaskA {
  static void Main() {
    int n = int.Parse(Console.ReadLine());
    int [] array = new int [n];
    string [] input = Console.ReadLine().Split(' ');
      
    for (int i = 0; i < n; i++) {
          array[i] = int.Parse(input[i]);
    }
          
    for (int i = 0; i < n/2; i++) {
        if (((2*(i+1) <= n)) && (array[i] <= array[2*i+1])) {
            continue;
        } else {
            Console.Write("NO");
            return;
        }
        
        if ((2*(i+1) + 1 <= n) && (array[i] <= array[2*i+2])) {
            continue;
        } else {
            Console.Write("NO");
            return;
        }
    }
    Console.Write("YES");
    }
}