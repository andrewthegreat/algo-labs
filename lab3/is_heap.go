package main

import (
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	inFile, _ := ioutil.ReadFile("isheap.in")
	outFile, _ := os.Create("isheap.out")

	line := strings.Split(string(inFile), "\n")[1]
	arrStr := strings.Fields(line)

	arr := make([]int, len(arrStr))
	for i := 0; i < len(arrStr); i++ {
		arr[i], _ = strconv.Atoi(string(arrStr[i]))
	}

	n := len(arr)

	for i := 0; i < n/2; i++ {
		if (2*(i+1) <= n) && (arr[i] <= arr[2*i+1]) {
			continue
		} else {
			outFile.WriteString("NO")
			return
		}

		if (2*(i+1)+1 <= n) && (arr[i] <= arr[2*i+2]) {
			continue
		} else {
			outFile.WriteString("NO")
		}
	}

	outFile.WriteString("YES")
	outFile.Close()
}
