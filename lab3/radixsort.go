package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func _max(arr []int) int {
	max := arr[0]
	for i := range arr {
		if arr[i] > max {
			max = arr[i]
		}
	}
	return max
}

func countSort(arr []int) []int {
	var result []int
	var count []int = make([]int, _max(arr)+1)

	for _, val := range arr {
		count[val] += 1
	}

	for i := 0; i < len(count); i++ {
		for j := 0; j < count[i]; j++ {
			result = append(result, i)
		}
	}
	return result
}

func main() {
	var arr []string
	var line []string

	inFile, _ := os.Open("radixsort.in")
	outFile, _ := os.Create("radixsort.out")
	defer outFile.Close()

	in := bufio.NewReader(inFile)
	rawLine, _ := in.ReadString('\n')
	line = strings.Fields(rawLine)

	m, _ := strconv.Atoi(line[1])
	k, _ := strconv.Atoi(line[2])

	scanner := bufio.NewScanner(inFile)
	scanner.Scan()
	for scanner.Scan() {
		arr = append(arr, scanner.Text())
	}

}
